//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Tubagus Uun on 14/08/20.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
