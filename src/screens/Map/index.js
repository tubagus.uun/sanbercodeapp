import styles from './style';
import React, { useEffect } from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { View, Text } from 'react-native';


MapboxGL.setAccessToken('pk.eyJ1IjoidHViYWd1c3V1biIsImEiOiJja2RmYXo2bzc0Z3JkMnJxdmx3Y2lsOW4yIn0.ESqFgI4wc1LPWZA9Dfamsg');

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]

const Map = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permision = await MapboxGL.requestAndroidLocationPermissions();

            } catch (err) {
                console.log("err useeffect ", err);
            }
        }
        getLocation();
    }, []);

    return (
        <View style={styles.container}>
            <MapboxGL.MapView style={styles.mapContainer}>
                <MapboxGL.UserLocation visible={true} />
                <MapboxGL.Camera followUserLocation={true} />

                {
                    coordinates.map((point, index) =>
                        <MapboxGL.PointAnnotation
                            id={index.toString()}
                            coordinate={point}
                            key={index}
                        >
                            <MapboxGL.Callout title={"Longitude: "+ point[0]+" Latitude: "+point[1]} />
                        </MapboxGL.PointAnnotation>
                    )}
            </MapboxGL.MapView>
        </View>
    );
}

export default Map;


