import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    TouchableWithoutFeedback,
    Modal
} from 'react-native';
import styles from './Register.style'
import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage';
import IonIcon from 'react-native-vector-icons/Ionicons';

const Register = ({ navigation }) => {

    const [isVisible, setIsVisible] = useState(false);
    const [type, setType] = useState('back');
    const [photo, setPhoto] = useState(null);
    const camv = useRef(null);

    const toggleCam = () => {
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePict = async () => {
        const options = { quality: 0.5, base64: true }
        if (camv && camv.current) {
            const data = await camv.current.takePictureAsync(options);
            setPhoto(data);
            setIsVisible(false);
        }
    }

    const uploadImage = (uri) => {
        console.log("urinya ",uri);
        const sessionId = new Date().getTime();
        const reference = storage().ref(`images/${sessionId}`).putFile(uri)
        .then( (response) => {
            alert('upload sukses');
        } )
        .catch( (error) => {
            console.log(error)
            alert(error);
        });
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible}
                animationType="slide"
                onRequestClose={() => setIsVisible(false)}>

                <View style={styles.container}>
                    <RNCamera
                        style={styles.rncamera}
                        ref={camv}
                        type={type}
                    >
                        <View style={styles.vbflip} >
                            <TouchableOpacity style={styles.tbflip} onPress={toggleCam} >
                                <IonIcon name={'camera-reverse'} size={40} color={"grey"} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.vround} />
                        <View style={styles.vrectangle} />
                        <View style={styles.vbsnap} >
                            <TouchableOpacity style={styles.tbsnap} onPress={takePict} >
                                <IonIcon name={'camera'} size={40} color={"grey"} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>

            </Modal>
        );
    }

    return (
        <View style={styles.container}>
            {renderCamera()}
            <View style={styles.profileContainer}>
                <Image style={styles.image} 
                source={photo === null ? require('../../assets/images/photo_2020-07-08_22-10-09.jpg') : {uri: photo.uri} } />
                <TouchableOpacity onPress={() => setIsVisible(true)} >
                    <Text style={styles.nama}>Change Picture</Text>
                </TouchableOpacity>

            </View>
            <View style={styles.ketContainer}>
            </View>

            <View style={styles.vAbout}>
                <View >
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Nama</Text>
                        <TextInput placeholder='Nama' style={styles.input}
                            autoCapitalize='none' />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput placeholder='Email' style={styles.input}
                            autoCapitalize='none' />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput placeholder='Password' style={styles.input} secureTextEntry={true}
                            autoCapitalize='none' />
                    </View>

                    <View style={styles.kotaklogin}>
                        <TouchableOpacity style={styles.btlogin}  
                        onPress={ () => uploadImage(photo.uri) } >
                            <Text style={styles.textbtlogin}> Register </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
}


export default Register;