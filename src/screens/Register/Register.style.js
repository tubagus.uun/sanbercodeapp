import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    profileContainer: {
        flex: 0.7,
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        alignItems: "center"
    },
    ketContainer: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 100,
    },
    nama: {
        color: '#fff',
        fontSize: 16,
        marginTop: 10
    },
    vAbout: {
        width: '85%',
        top: "35%",
        borderRadius: 100,
        backgroundColor: '#ffff',
        borderRadius: 10,
        alignSelf: 'center',
        position: 'absolute',
        paddingVertical: 15,
        paddingHorizontal: 15,
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    forminput: {
        marginVertical: 5,
        alignContent: 'center',
    },
    input: {
        height: 40,
        borderColor: 'grey',
        borderBottomWidth: 0.5,
        marginBottom: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 30,
        marginBottom: 10,
    },
    textbtlogin: {
        color: 'white',
        textAlign: "center",
        fontSize: 12,
        fontWeight: "bold",
    },
    rncamera: {
        flex: 1,
    },
    vbflip: {
        flex: 0.5,
        margin: 20,
        borderColor: 'grey',
    },
    tbflip :{
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
    },
    vround: {
        flex: 1,
        width: 120,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'grey',
        alignSelf: "center",
        marginBottom: 50
    },
    vrectangle: {
        flex: 0.5,
        width: 150,
        borderWidth: 1,
        borderColor: 'grey',
        alignSelf: "center",
    },
    vbsnap: {
        flex: 1,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    tbsnap:{
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
    }

})

export default styles;