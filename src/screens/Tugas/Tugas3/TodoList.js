import React, { useState } from 'react';
import {
    StyleSheet,
    Platform,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Alert,
    FlatList,
    SafeAreaView
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


function Item({ item, hapusmethod }) {
    return (
        <View style={styles.vtodolist}>
            <View style={styles.vnote}>
                <Text style={styles.tnote}> {item.tgl} </Text>
                <Text style={styles.tnote}> {item.isi} </Text>
            </View>
            <View style={styles.vBhapus}>
                <TouchableOpacity style={styles.bhapus}
                    onPress={hapusmethod} >
                    <FontAwesome name={'trash-o'} size={20} />
                </TouchableOpacity>
            </View>

        </View>
    );
}

const App = () => {

    const [arrText, setArrText] = useState([]);
    const [isi, setIsi] = useState('');

    const pressTambah = () => {
        let d = new Date();
        let a = {
            tgl: d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear(),
            isi: isi
        }
        setArrText([...arrText, a]);
        setIsi('')
    }

    const hapus = (index) => {
        arrText.splice(index, 1)
        setArrText([...arrText])
    }

    const renderItem = ({ index, item }) => {
        return (
            <Item item={item} hapusmethod={hapus.bind(null, index)} />
        );
    };

    return (
        <View style={styles.container}>
            <Text>Masukan TodoList</Text>
            <View style={styles.vInput}>
                
                <TextInput style={styles.tInput}
                    onChangeText={
                        (val) => setIsi(val)
                    } value={isi} />
                <TouchableOpacity style={styles.bplus} onPress={pressTambah}>
                    <Ionicons name={'add'} size={20} />
                </TouchableOpacity>
            </View>
            <FlatList
                data={arrText} renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        ...Platform.select({
            ios: {
                padding: 10,
                paddingTop:20
            },
            android: {
                padding: 10,
            },
            default: {
                padding: 5,
            }
        })
    },
    vInput: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    tInput: {
        flex:1,
        marginRight:5,
        borderWidth: 2,
        borderRadius: 5,
        ...Platform.select({
            ios: {
                paddingVertical:15
            }
        })
    },
    bplus: {
        backgroundColor: 'powderblue',
        width: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bhapus: {

    },
    vtodolist: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 4,
        padding: 10,
        paddingVertical:20,
        paddingRight: 25,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: 'grey',
    },
    vnote: {
        // borderWidth: 1,
    },
    tnote: {
        fontSize: 11
    },
    vBhapus: {
        alignItems: 'center',
        justifyContent: 'center',
        // borderWidth: 1
    },
});
export default App;