import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';


const App = () => {
    return (
        <View style={styles.container}>
            <Text>Hallo Kelas React Native Lanjutan SanberCode!</Text>
        </View>
    );
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
});

export default App;