import React, { useState, createContext } from 'react';
import TodoList from './TodoList'

export const RootContext = createContext();

const Context = () => {

    const [arrText, setArrText] = useState([]);
    const [isi, setIsi] = useState('');

    handleChangeIsi = (val) => {
        setIsi(val);
    }

    addArrText = () => {
        let d = new Date();
        let a = {
            tgl: d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear(),
            isi: isi
        }
        setArrText([...arrText, a]);
        setIsi('');
    }

    hapusArrText = (index) => {
        arrText.splice(index, 1)
        setArrText([...arrText])
    }

    return (
        <RootContext.Provider value={{
            isi,
            arrText,
            handleChangeIsi,
            addArrText,
            hapusArrText
        }}>
            <TodoList />
        </RootContext.Provider>
    );
}

export default Context;