import React, { useState, useContext } from 'react';
import {
    StyleSheet,
    Platform,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Alert,
    FlatList,
    SafeAreaView
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {RootContext} from "./index"

const Item = ({ item, indek }) => {
    const state = useContext(RootContext);
    return (
        <View style={styles.vtodolist}>
            <View style={styles.vnote}>
                <Text style={styles.tnote}> {item.tgl} </Text>
                <Text style={styles.tnote}> {item.isi} </Text>
            </View>
            <View style={styles.vBhapus}>
                <TouchableOpacity style={styles.bhapus}
                    onPress={ () => {state.hapusArrText(indek)} }
                     >
                    <FontAwesome name={'trash-o'} size={20} />
                </TouchableOpacity>
            </View>

        </View>
    );
}

const App = () => {

    const state = useContext(RootContext);

    const renderItem = ({ item, index }) => {
        return (
            <Item item={item} indek={index}  />
        );
    };

    return (
        <View style={styles.container}>
            <Text>Masukan TodoList {Platform.OS + " - " + Platform.Version} </Text>
            <View style={styles.vInput}>
                
                <TextInput style={styles.tInput}
                    onChangeText={
                        (val) => state.handleChangeIsi(val)
                    } value={state.isi} />
                <TouchableOpacity style={styles.bplus} onPress={state.addArrText}>
                    <Ionicons name={'add'} size={20} />
                </TouchableOpacity>
            </View>
            <FlatList
                data={state.arrText} renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        ...Platform.select({
            ios: {
                padding: 10,
                paddingTop:20
            },
            android: {
                padding: 10,
            },
            default: {
                padding: 5,
            }
        })
    },
    vInput: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    tInput: {
        flex:1,
        marginRight:5,
        borderWidth: 2,
        borderRadius: 5,
        ...Platform.select({
            ios: {
                paddingVertical:15
            }
        })
    },
    bplus: {
        backgroundColor: 'powderblue',
        width: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bhapus: {

    },
    vtodolist: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 4,
        padding: 10,
        paddingVertical:20,
        paddingRight: 25,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: 'grey',
    },
    vnote: {
        // borderWidth: 1,
    },
    tnote: {
        fontSize: 11
    },
    vBhapus: {
        alignItems: 'center',
        justifyContent: 'center',
        // borderWidth: 1
    },
});
export default App;