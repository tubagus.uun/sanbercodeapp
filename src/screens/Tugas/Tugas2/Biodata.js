import React from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const App = () => {
    return (
        <View style={styles.container}>
            <View style={styles.profileContainer}>
                <Image style={styles.image} source={require('../../../assets/images/photo_2020-07-08_22-10-09.jpg')} />
                <Text style={styles.nama}>Tubagus Uun</Text>
            </View>
            <View style={styles.ketContainer}>
            </View>
            <View style={styles.vAbout}>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Tanggal Lahir</Text>
                    <Text style={styles.tDetail}>09 Juni 1983</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Jenis Kelamin</Text>
                    <Text style={styles.tDetail}  >laki-Laki</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Hobi</Text>
                    <Text style={styles.tDetail}  >Makan</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>No Telp</Text>
                    <Text style={styles.tDetail}  >081219654445</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Email</Text>
                    <Text style={styles.tDetail}  >tubagus.uun@gmail.com</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profileContainer: {
        flex: 0.7,
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        alignItems: "center"
    },
    ketContainer: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    image: {
        width: wp(28),
        height: hp(15),
        borderRadius: 100,
    },
    nama: {
        color: '#fff',
        fontSize: hp(2),
        marginTop:10
    },
    vAbout: {
        width: '85%',
        top: "35%",
        borderRadius: 100,
        backgroundColor: '#ffff',
        borderRadius: 10,
        alignSelf: 'center',
        position: 'absolute',
        paddingVertical: 15,
        paddingHorizontal: 15,
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    vDetailAbout: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    tDetail: {
        fontSize: 11,
        padding:7

    },
});

export default App;