import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    buttonCircle: {
        width: 44,
        height: 44,
        backgroundColor: '#191970',
        borderRadius: 22,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#22bcb5',
    },
    title: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center',
    },
    image: {
        width: 320,
        height: 320,
        marginVertical: 32,
    },
    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        textAlign: 'center',
    },
    forminput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
    },
    formtext: {

    },
    input: {
        height: 40,
        borderColor: 'grey',
        borderBottomWidth: 1,
        marginBottom: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 30,
        marginBottom: 10,
    },
    btfinger:{
        alignItems: "center",
        backgroundColor: "#191970",
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 30,
        marginBottom: 10,
    },
    textbtlogin: {
        color: 'white',
        textAlign: "center",
        fontSize: 12,
        fontWeight: "bold",
    },
    vbatas: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        marginBottom: 10,
        marginHorizontal: 30,
    },
    vb: {
        flex:1,
        borderBottomWidth: 1,
    },
    vgoogle: {
        alignItems: 'center',
        paddingHorizontal:30
    },
    btgoogle: {
        alignItems: "center",
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 30,
        marginBottom: 10,
    },
    vreg: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 20
    },
    innerText: {
        color: 'red'
    }
})

export default styles;