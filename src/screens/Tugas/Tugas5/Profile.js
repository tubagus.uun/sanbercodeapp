import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';
import {
    GoogleSignin,
} from '@react-native-community/google-signin';

const App = ({ navigation }) => {

    const [userInfo, setUserInfo] = useState(null);
    const { getItem, setItem, removeItem } = useAsyncStorage('token');

    useEffect( () => {
        console.log(userInfo)
        const getToken = async () => {
            let key = await getItem();
            console.log("key : ", key);
        };
        getToken();
        getCurrentUser();
    }, [] );

    const getCurrentUser = async () => { 
        const user = await GoogleSignin.signInSilently();
        console.log("userInfo ", user);
        setUserInfo(user);
     }

    const logOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            await AsyncStorage.clear();
            console.log("clear");
        } catch (err) {
            console.log(err)
        }
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{ name: "Login" }]
            })
        );
    }

    return (
        <View style={styles.container}>
            <View style={styles.profileContainer}>
                <Image style={styles.image} source={{uri : userInfo && userInfo.user && userInfo.user.photo }} />
                <Text style={styles.nama}>{userInfo && userInfo.user && userInfo.user.name}</Text>
            </View>
            <View style={styles.ketContainer}>
            </View>
            <View style={styles.vAbout}>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Tanggal Lahir</Text>
                    <Text style={styles.tDetail}>09 Juni 1983</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Jenis Kelamin</Text>
                    <Text style={styles.tDetail}  >laki-Laki</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Hobi</Text>
                    <Text style={styles.tDetail}  >Makan</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>No Telp</Text>
                    <Text style={styles.tDetail}  >081219654445</Text>
                </View>
                <View style={styles.vDetailAbout}>
                    <Text style={styles.tDetail}>Email</Text>
                    <Text style={styles.tDetail}  >{userInfo && userInfo.user && userInfo.user.email}</Text>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin} onPress={logOut} >
                        <Text style={styles.textbtlogin}> Log Out </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profileContainer: {
        flex: 0.7,
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        alignItems: "center"
    },
    ketContainer: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 100,
    },
    nama: {
        color: '#fff',
        fontSize: 16,
        marginTop: 10
    },
    vAbout: {
        width: '85%',
        top: "35%",
        borderRadius: 100,
        backgroundColor: '#ffff',
        borderRadius: 10,
        alignSelf: 'center',
        position: 'absolute',
        paddingVertical: 15,
        paddingHorizontal: 15,
        elevation: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    vDetailAbout: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    tDetail: {
        fontSize: 11,
        padding: 7

    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 30,
        marginBottom: 10,
    },
    textbtlogin: {
        color: 'white',
        textAlign: "center",
        fontSize: 12,
        fontWeight: "bold",
    },
});

export default App;