import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import SplashScreen from './SplashScreen';
import Intro from './Intro';
import Login from './Login';
import Register from './Register';
import Profile from './Profile';
import Home from './Home';

const Stack = createStackNavigator();
const TabsStack = createBottomTabNavigator();

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if (route.name == 'Home') {
                return <FontAwesome5 name={'centos'} size={size} color={color} solid />
            } else if (route.name == 'Profile') {
                return <FontAwesome5 name="globe-asia" size={size} color={color} solid />
            } 
        },
    });
}

const TabsStackScreen = ({ navigation, route }) => {
    return (
        <TabsStack.Navigator screenOptions={iconTab} >
            <TabsStack.Screen name='Home' component={Home}
                options={{
                    title: 'Home',
                }} />
            <TabsStack.Screen name='Profile' component={Profile}
                options={{
                    title: 'Profile',
                }} />
        </TabsStack.Navigator>
    );
}

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
        <Stack.Screen name="TabsStackScreen" component={TabsStackScreen}  />
    </Stack.Navigator>
)

function AppNavigation() {
    const [isLoading, setIsLoading] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 2000)
    }, [])

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;