import React from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    Dimensions,
} from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import styles from './Home.style';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const App = (props) => {

    const goToReactNative = () => {
        props.navigation.navigate('ReactNative');
    }

    const sizeIcon = hp("7.5%");
    return (
        <ScrollView style={styles.scrollContainer}>
            <View style={styles.container}>

                <View style={styles.vkelas}>
                    <View style={styles.vkelasjudul}>
                        <Text style={styles.tkelasjudul}>Kelas</Text>
                    </View>
                    <View style={styles.vkelasisi}>
                        <TouchableOpacity onPress={goToReactNative} >
                            <View style={styles.vkelasisicon}>
                                <IonIcon name={'logo-react'} size={sizeIcon} color={"white"} />
                                <Text style={styles.tkelas}>React Native</Text>
                            </View>
                        </TouchableOpacity>

                        <View style={styles.vkelasisicon}>
                            <IonIcon name={'logo-python'} size={sizeIcon} color={"white"} />
                            <Text style={styles.tkelas}>Data Science</Text>
                        </View>
                        <View style={styles.vkelasisicon}>
                            <IonIcon name={'logo-react'} size={sizeIcon} color={"white"} />
                            <Text style={styles.tkelas}>React JS</Text>
                        </View>
                        <View style={styles.vkelasisicon}>
                            <IonIcon name={'logo-laravel'} size={sizeIcon} color={"white"} />
                            <Text style={styles.tkelas}>Laravel</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.vkelas}>
                    <View style={styles.vkelasjudul}>
                        <Text style={styles.tkelasjudul}>Kelas</Text>
                    </View>
                    <View style={styles.vkelasisi}>
                        <View style={styles.vkelasisicon}>
                            <IonIcon name={'logo-wordpress'} size={sizeIcon} color={"white"} />
                            <Text style={styles.tkelas}>React Native</Text>
                        </View>
                        <View style={styles.vkelasisicon}>
                            <Image style={styles.imageicon} source={require('../../assets/images/website-design.png')} />
                            <Text style={styles.tkelas}>Design Grafis</Text>
                        </View>
                        <View style={styles.vkelasisicon}>
                            <MaterialCommunityIcon name={'server'} size={sizeIcon} color={"white"} />
                            <Text style={styles.tkelas}>Web Server</Text>
                        </View>
                        <View style={styles.vkelasisicon}>
                            <Image style={styles.imageicon} source={require('../../assets/images/ux.png')} />
                            <Text style={styles.tkelas}>UI/UX Design</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.vsummary}>
                    <View style={styles.vsumjudul}>
                        <Text style={styles.tsumjudul}>Summary</Text>
                    </View>
                    <View style={styles.vsumsubjudul}>
                        <Text style={styles.tsumsubjudul}>React Native</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Today</Text>
                        <Text style={styles.tsumisi}>20 Orang</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Total</Text>
                        <Text style={styles.tsumisi}>100 Orang</Text>
                    </View>
                    <View style={styles.vsumsubjudul}>
                        <Text style={styles.tsumsubjudul}>Data Science</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Today</Text>
                        <Text style={styles.tsumisi}>30 Orang</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Total</Text>
                        <Text style={styles.tsumisi}>100 Orang</Text>
                    </View>
                    <View style={styles.vsumsubjudul}>
                        <Text style={styles.tsumsubjudul}>ReactJS</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Today</Text>
                        <Text style={styles.tsumisi}>66 Orang</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Total</Text>
                        <Text style={styles.tsumisi}>100 Orang</Text>
                    </View>
                    <View style={styles.vsumsubjudul}>
                        <Text style={styles.tsumsubjudul}>Laravel</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Today</Text>
                        <Text style={styles.tsumisi}>60 Orang</Text>
                    </View>
                    <View style={styles.vsumisi}>
                        <Text style={styles.tsumisi}>Total</Text>
                        <Text style={styles.tsumisi}>100 Orang</Text>
                    </View>
                </View>
            </View>
        </ScrollView>

    );
}

export default App;