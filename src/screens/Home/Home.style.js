import { StyleSheet, Dimensions } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: wp("2%")
    },
    scrollContainer: {
        flex: 1,
        flexGrow: 1,
    },
    vkelas: {
        height: hp("16.5%"),
        marginVertical: wp("1.2%"),
        borderRadius: wp("2%"),
    },
    vkelasjudul: {
        height: hp("5%"),
        backgroundColor: '#088dc4',
        borderTopStartRadius: wp("2%"),
        borderTopEndRadius: wp("2%"),
        justifyContent: "center",
        paddingHorizontal: wp("2%"),
    },
    vkelasisi: {
        flex: 1,
        backgroundColor: '#3EC6FF',
        borderBottomStartRadius: wp("2%"),
        borderBottomEndRadius: wp("2%"),
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'baseline',
        paddingTop: hp("0.6%"),
    },
    vkelasisicon: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    tkelasjudul: {
        color: 'white',
        fontSize: hp("1.7%"),
        fontWeight: 'bold'
    },
    tkelas: {
        color: 'white',
        fontSize: hp("1.4%"),
    },
    imageicon: {
        width: hp("7.5%"),
        height: hp("7.5%"),
        resizeMode: 'contain',
    },
    vsummary: {
        flex: 1,
        borderRadius: wp("2%"),
        marginTop: hp("0.6%"),
    },
    vsumjudul: {
        height: hp("5%"),
        backgroundColor: '#088dc4',
        borderTopStartRadius: wp("2%"),
        borderTopEndRadius: wp("2%"),
        justifyContent: "center",
        paddingHorizontal: wp("2%"),
    },
    tsumjudul: {
        color: 'white',
        fontSize: hp("1.7%"),
    },
    vsumsubjudul: {
        height: hp("5%"),
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        paddingHorizontal: wp("2%"),
    },
    tsumsubjudul: {
        color: 'white',
        fontSize: hp("1.7%"),
        fontWeight: 'bold'
    },
    vsumisi: {
        height: hp("5%"),
        backgroundColor: '#088dc4',
        paddingHorizontal: wp("10%"),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    tsumisi: {
        color: 'white',
        fontSize: hp("1.5%"),
    },
});

export default styles;