import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  processColor,
  StyleSheet
} from 'react-native';
import styles from './style';
import { BarChart } from 'react-native-charts-wrapper';
import { Colors } from 'react-native/Libraries/NewAppScreen';

let databaru = [];
const ReactNative = ({ navigation }) => {

  const data = [
    { y: [100, 40], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [80, 60], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [40, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [78, 45], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [67, 87], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [98, 32], marker: ["React Native Dasar", "React Native Lanjutan"] },
    { y: [150, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
  ]

  useEffect(() => {

    data.map((x) => {
      let mar = {
        marker: [`rn dasar ${x.y[0]}`, `rn lanjutan ${x.y[1]}`]
      }
      let db = { ...x, ...mar };
      databaru.push(db);
    });
    console.log(databaru);
    const dbaru = {
      data: {
        dataSets: [{
          values: databaru,
          label: '',
          config: {
            colors: [processColor('blue'), processColor('darkblue')],
            stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
            drawFilled: false,
            drawValues: false,
          }
        }]
      }
    }
    setChart(dbaru);


  }, []);

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5
  })
  const [chart, setChart] = useState({
    data: {
      dataSets: [{
        values: [],
        label: '',
      }]
    }
  })

  const [xAxis, setXAxis] = useState({
    valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
  })
  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false
    }
  })
  const [highlights, setHighlights] = useState([{ x: 3 }, { x: 6 }]);

  console.log("ok", chart.data);
  return (
    <View style={styles.container}>
      <BarChart
        style={{ flex: 1 }}
        data={chart.data}
        xAxis={xAxis}
        yAxis={yAxis}
        legend={legend}
        animation={{ durationX: 2000 }}

        gridBackgroundColor={processColor('#ffffff')}
        visibleRange={{ x: { min: 5, max: 5 } }}
        drawBarShadow={false}
        drawValueAboveBar={true}
        drawHighlightArrow={true}
        doubleTapToZoomEnabled={false}
        // onSelect={this.handleSelect.bind(this)}
        highlights={highlights}
        marker={{
          enabled: true,
          markerColor: processColor('grey'),
          textColor: processColor('white'),
          textSize: 14,
        }}
        onChange={(event) => console.log("", event.nativeEvent)}
      />
    </View>
  );
}


export default ReactNative;