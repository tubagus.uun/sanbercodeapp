import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    processColor,
    StyleSheet
} from 'react-native';
import styles from './style';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat';


const Chat = (props) => {

    const [messages, setMessages] = useState([]);
    const [user, setUser] = useState({});

    useEffect(() => {
        const user = auth().currentUser;
        console.log("Chat -> user", user)
        setUser(user);
        getData();
        return () => {
            const db = database().ref("messages")
            if (db) {
                db.off()
            }
        }
    }, []);

    const getData = () => {
        database().ref("messages").limitToLast(20)
            .on("child_added", snapshot => {
                const value = snapshot.val();
                console.log("value ", value);
                setMessages(previousMessages => GiftedChat.append(previousMessages, value));

            })
    }

    const onSend = (messages) => {
        console.log("dikirim ", messages);
        for (let i = 0; i < messages.length; i++) {
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    }
    return (

        <GiftedChat
            messages={messages}
            onSend={onSend}
            user={{
                _id: user.uid,
                name: user.email,
            }}
        />
    );
}

export default Chat;