import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    StyleSheet,
    Alert,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    TouchableWithoutFeedback
} from 'react-native';
import styles from './style';
import { CommonActions } from '@react-navigation/native'

import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth'
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id'

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Senseor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'

}

const Login = ({ navigation, route }) => {

    console.log("Login -> route", route)

    const [isPassword, setIsPassword] = useState(true);
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const { getItem, setItem, removeItem } = useAsyncStorage('token');

    useEffect(() => {
        configureGoogleSignIn();
        GetKey();

    }, []);

    const SetKey = async (key) => {
        try {
            await setItem(key);
            console.log("ok disimpen");
        } catch (err) {
            console.log(err);
        }

    }

    const GetKey = async () => {
        let key = await getItem();
        if(key != null ){
            navigation.reset({
                index: 0,
                routes: [{ name: 'TabsStackScreen', param: { screen: 'Home' } }]
            });
        }
    }

    const ClearAll = async () => {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        await AsyncStorage.clear();
    }

    const loginHandler = () => {

        // navigation.dispatch(
        //     CommonActions.reset({
        //         index: 0,
        //         routes: [{ name: 'TabsStackScreen', param: { screen: 'Home' } }]
        //     })
        // );

        // navigation.reset({
        //     index: 0,
        //     routes: [{ name: 'TabsStackScreen', param: { screen: 'Home' } }]
        // });

        if (userName == '' || password == '') {
            Alert.alert('Error', 'Username dan Password Harus diisi');
        } else {
            SignInAction(userName, password);
        }
    }


    const SignInAction = async (email, password) => {
        return auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                navigation.navigate('TabsStackScreen', { screen: 'Home' });
            })
            .catch((err) => {
                console.log("error ", err);
            })
    }

    // const SignInAction = async (username, passsword) => {

    //     let url = "https://mainbersama.demosanbercode.com/api/login";
    //     const cheaders = {
    //         'Content-Type': 'application/json',
    //         Accept: 'application/json',
    //     };
    //     const initPost = {
    //         method: 'POST'
    //     };
    //     let headers = { headers: { ...cheaders } };
    //     let data = {
    //         email: username,
    //         password: passsword
    //     };
    //     let body = { body: JSON.stringify(data) };
    //     let initReq = { ...initPost, ...headers, ...body };
    //     console.log(initReq)
    //     try {
    //         const response = await fetch(url, initReq);
    //         if (response.status != 200) {
    //             let err = new Error("Error BOY")
    //             err.response = response
    //             throw err
    //         }
    //         const json = await response.json();

    //         let token = json.token;
    //         console.log(token);
    //         await SetKey(token);

    //         navigation.reset({
    //             index: 0,
    //             routes: [{ name: 'TabsStackScreen', param: { screen: 'Home' } }]
    //         });

    //         // navigation.navigate('TabsStackScreen', { screen: 'Profile' });
    //     } catch (err) {
    //         const errText = await err.response.json();
    //         alert(errText.message)
    //     } finally {
    //         console.log("finnaly")
    //     }
    // }

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '365936067783-kijdmugsr1p3lohtppa3tj635fp8hn6g.apps.googleusercontent.com'
        });
    }

    const SignInWithGoogle = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const { idToken } = await GoogleSignin.signIn();
            console.log("idToken -> ", idToken);
            const credential = auth.GoogleAuthProvider.credential(idToken);
            auth().signInWithCredential(credential);
            await SetKey(idToken);
            navigation.navigate('TabsStackScreen', { screen: 'Profile' });

        } catch (error) {
            console.log("error ", error);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    }

    const signInWithFingerPrint = () => {
        TouchID.authenticate('', config)
            .then(success => {
                console.log(success)
                navigation.navigate('TabsStackScreen', { screen: 'Profile' });
            })
            .catch(error => {
                alert('Authentication Failed')
            })
    }

    const gotoRegister = () => {
        navigation.navigate('Register');
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container} >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    <Image source={require('../../assets/images/logo.jpg')} />
                    <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Username / Email</Text>
                        <TextInput placeholder='Username or Email' style={styles.input}
                            onChangeText={userName => { setUserName(userName) }}
                            value={userName} autoCapitalize='none' />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput placeholder='Password' style={styles.input} secureTextEntry={true}
                            onChangeText={password => { setPassword(password) }}
                            value={password} autoCapitalize='none' />
                    </View>
                    <View style={styles.kotaklogin}>
                        <TouchableOpacity style={styles.btlogin} onPress={loginHandler} >
                            <Text style={styles.textbtlogin}>  Test Code-push </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.vbatas}>
                        <View style={styles.vb} />
                        <Text> OR </Text>
                        <View style={styles.vb} />
                    </View>
                    <View style={styles.vgoogle}>
                        <TouchableOpacity onPress={SignInWithGoogle} >
                            <GoogleSigninButton
                                onPress={() => { }}
                                style={styles.btgoogle}
                                size={GoogleSigninButton.Size.Wide}
                                color={GoogleSigninButton.Color.Dark}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.kotaklogin}>
                        <TouchableOpacity style={styles.btfinger} onPress={signInWithFingerPrint} >
                            <Text style={styles.textbtlogin}>  SIGN IN WITH FINGERPRINT </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.vreg}>
                        <Text >Belum mempunyai akun ?
                        <Text style={styles.innerText}
                                onPress={gotoRegister} >  Buat Akun </Text>
                        </Text>
                    </View>

                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>

    )
}

export default Login;