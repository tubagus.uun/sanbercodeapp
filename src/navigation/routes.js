import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';

import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';

import SplashScreen from '../screens/SplashScreen/SplashScreen';
import Intro from '../screens/Intro/Intro';
import Login from '../screens/Login/Login';
import Register from '../screens/Register/Register';
import Profile from '../screens/Profile/Profile';
import Home from '../screens/Home/Home';
import Map from '../screens/Map';
import ReactNative from '../screens/ReactNative';
import Chat from '../screens/Chat'


const Stack = createStackNavigator();
const TabsStack = createBottomTabNavigator();

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if (route.name == 'Home') {
                return <FontAwesome5 name={'centos'} size={size} color={color} solid />
            } else if (route.name == 'Map') {
                return <FontAwesome5 name="map-marker-alt" size={size} color={color} solid />
            } else if (route.name == 'Chat') {
                return <IonIcon name="ios-chatbubbles" size={size} color={color} solid />
            } else if (route.name == 'Profile') {
                return <FontAwesome5 name="globe-asia" size={size} color={color} solid />
            }
        },
    });
}

const TabsStackScreen = ({ navigation, route }) => {
    return (
        <TabsStack.Navigator screenOptions={iconTab} >
            <TabsStack.Screen name='Home' component={Home}
                options={{
                    title: 'Home',
                }} />
            <TabsStack.Screen name='Map' component={Map}
                options={{
                    title: 'Map',
                }} />
            <TabsStack.Screen name='Chat' component={Chat}
                options={{
                    title: 'Chat',
                }} />
            <TabsStack.Screen name='Profile' component={Profile}
                options={{
                    title: 'Profile',
                }} />
        </TabsStack.Navigator>
    );
}

const MainNavigation = () => {
    const [isIntro, setIsIntro] = React.useState(null);
    const [token, setToken] = React.useState(null);

    console.log("MainNavigation -> token", token)
    console.log("MainNavigation -> isIntro", isIntro)

    React.useEffect(() => {

        const cekIntro = async () => {
            let intro = await AsyncStorage.getItem("intro");
            let tok = await AsyncStorage.getItem("token");
            setIsIntro(intro);
            setToken(tok);
        }
        cekIntro();
        console.log("MainNavigation -> isIntro", isIntro);
        console.log("MainNavigation -> token", token);
    }, [])

    return (
        <Stack.Navigator>
            {
                (isIntro == null) ? (
                    <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
                ) : null
            }
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="TabsStackScreen" component={TabsStackScreen} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="ReactNative" component={ReactNative} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

function AppNavigation() {
    const [isLoading, setIsLoading] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 2000)
    }, [])

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;